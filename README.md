# Docker Symfony

A Dockerfile for [Symfony](http://symfony.com/) Web container: Nginx, PHP-FPM�

This image is ready for MySQL, SQLite & PostgreSQL. Feel free to connect with your required database system.

**This configuration is built for development. You can use it in production at your own risks !**

## Installation

Run following command to build & run container:

```
docker run -d -p 80:80 unsw/docker-ci
```

Your project is available at [http://127.0.0.1](http://127.0.0.1).

 